import 'package:intl/intl.dart';

class DateFormatter {
  final DateFormat _dateData;

  DateFormatter(this._dateData);

  static String getDate(_dateData) => DateFormat().format(_dateData);
}
