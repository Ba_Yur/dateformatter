import 'package:flutter/material.dart';
import './utils/dateFormat.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',

      home: MyHomePage(),
    );
  }
}


class MyHomePage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('DateFormatter'),
      ),
      body: Container(
        child: Text(
            DateFormatter.getDate(DateTime.now())
        ),
      ),
    );
  }
}

